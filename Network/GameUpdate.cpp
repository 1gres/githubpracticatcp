#include "GameUpdate.h"



bool GameUpdate::isPosOccupied(Pos pos)
{
	for (map<int, PlayerData>::iterator it = playersDataInfo.begin(); it != playersDataInfo.end(); ++it) {
		if (it->second.getPos() == pos) return true;
	}
	return false;
}

int GameUpdate::getPlayerIdFromLocation(Pos pos)
{
	int playerId = -1;
	for (map<int, PlayerData>::iterator it = playersDataInfo.begin(); it != playersDataInfo.end(); ++it) {
		if (it->second.getPos() == pos) return it->first;
	}
	return playerId;
}

GameUpdate::GameUpdate()
{
}


GameUpdate::~GameUpdate()
{
}

void GameUpdate::addUser(int playerId)
{
	PlayerData playerData(playerId);
	if (playersDataInfo.size() == 0) {
		playerData.setPos(Pos(25, 25));
	}
	else if (playersDataInfo.size() == 1) {
		playerData.setPos(Pos(75, 75));
	}
	else if (playersDataInfo.size() == 2) {
		playerData.setPos(Pos(25, 75));
	}
	else if (playersDataInfo.size() == 3) {
		playerData.setPos(Pos(75, 25));
	}
}

void GameUpdate::changeMunicioLocation()
{
	int numColum, numFil;
	do {
		numColum = rand() % width;
		numFil = rand() % height;
	} while (!isPosOccupied(Pos(numFil, numColum)));
	municioLocation = Pos(numColum, numFil);
	
}

void GameUpdate::setPlayerMunicio(int playerId, int newMunicio)
{
	playersDataInfo[playerId].setMunicio(newMunicio);
}

void GameUpdate::setPlayerHp(int playerId, int newHp)
{
	playersDataInfo[playerId].setActualHp(newHp);
}

void GameUpdate::movePlayer(int playerId, TypeMove typeMove)
{
	Pos actualPos = getPlayerLocation(playerId);
	switch (typeMove)
	{
	case MOVE_UP:
		actualPos.first -= 1;
		setPlayerLocation(playerId, actualPos);
		break;

	case MOVE_DOWN:
		actualPos.first += 1;
		setPlayerLocation(playerId, actualPos);
		break;

	case MOVE_LEFT:
		actualPos.second -= 1;
		setPlayerLocation(playerId, actualPos);
		break;

	case MOVE_RIGHT:
		actualPos.second += 1;
		setPlayerLocation(playerId, actualPos);
		break;

	default:
		break;
	}
}

bool GameUpdate::isPlayerDead(int playerId)
{
	return playersDataInfo[playerId].isDead();
}

int GameUpdate::playerIdByPos(int pos)
{
	map<int, PlayerData>::iterator it = playersDataInfo.begin();
	for (int i = 0; i <= pos; ++i) {
		++it;
	}
	return it -> first;
}

void GameUpdate::setPlayerLocation(int playerId, Pos newLocation)
{
	playersDataInfo[playerId].setPos(newLocation);
}

void GameUpdate::shootLong(int playerId, Pos objectivePos)
{
	int enemyId = getPlayerIdFromLocation(objectivePos);
	if (enemyId != -1) {
		playersDataInfo[enemyId].setActualHp(playersDataInfo[enemyId].getActualHp() - damagePerLongAttack);
		playersDataInfo[playerId].setMunicio(playersDataInfo[playerId].getMunicio() - 1);
	}
}

void GameUpdate::shootArea(int playerId, Pos attackCenterPos)
{
	for (int i = attackCenterPos.first - areaAttackRange; i < attackCenterPos.first + areaAttackRange; ++i) {
		for (int j = attackCenterPos.second - areaAttackRange; j < attackCenterPos.second + areaAttackRange; ++j) {
			int enemyId = getPlayerIdFromLocation(Pos(i, j));
			if (enemyId != -1) {
				playersDataInfo[enemyId].setActualHp(playersDataInfo[enemyId].getActualHp() - damagePerLongAttack);
			}
		}
	}
	playersDataInfo[playerId].setMunicio(playersDataInfo[playerId].getMunicio() - 1);
}

void GameUpdate::movePlayer(int playerId, Pos newPos)
{
	playersDataInfo[playerId].setPos(newPos);
	if (newPos == municioLocation) {
		changeMunicioLocation();
		playersDataInfo[playerId].setMunicio(playersDataInfo[playerId].getMunicio() + 1);
	}
}

Pos GameUpdate::getPlayerLocation(int playerId)
{
	return playersDataInfo[playerId].getPos();
}

int GameUpdate::getPlayerMunicio(int playerId)
{
	return playersDataInfo[playerId].getMunicio();
}

int GameUpdate::getPlayerHp(int playerId)
{
	return playersDataInfo[playerId].getActualHp();
}

sf::Packet GameUpdate::createPacket()
{
	sf::Packet pack;

	pack << GAME_UPDATE;

	pack << municioLocation.first << municioLocation.second;

	pack << playersDataInfo.size();
	for (map<int, PlayerData>::iterator it = playersDataInfo.begin(); it != playersDataInfo.end(); ++it) {
		pack << it->first;
		pack << it->second.getActualHp();
		pack << it->second.getMunicio();
		pack << it->second.getPos().first;
		pack << it->second.getPos().second;
	}

	return pack;
}

void GameUpdate::createFromPacket(sf::Packet pack)
{
	pack >> municioLocation.first >> municioLocation.second;

	int n;
	pack >> n;
	for (int i = 0; i < n; ++i) {
		PlayerData player;
		player.createFromPacket(pack);
		playersDataInfo[player.getPlayerId()] = player;
	}
}
