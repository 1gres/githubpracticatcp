#pragma once
#include "PlayerData.h"
#include <list>


using namespace std;


class GameUpdate
{
private:
	
	Pos municioLocation;
	bool isPosOccupied(Pos pos);
	int getPlayerIdFromLocation(Pos pos);


public:

	map<int, PlayerData> playersDataInfo;

	const static int width = 100;
	const static int height = 100;

	const static int maxPointPerTurn = 5;
	const static int costPerMove = 1;
	const static int costPerAttack = 2;
	const static int damagePerLongAttack = 100;
	const static int damagePerAreaAttack = 50;
	const static int areaAttackRange = 5;

	const static int maxTurnsMunicioPosition = 10;

	GameUpdate();
	~GameUpdate();

	void addUser(int playerId);
	void changeMunicioLocation();

	void setPlayerMunicio(int playerId, int newMunicio);
	void setPlayerHp(int playerId, int newHp);
	void setPlayerLocation(int playerId, Pos newLocation);

	void movePlayer(int playerId, TypeMove typeMove);
	bool isPlayerDead(int playerId);

	int playerIdByPos(int pos);
	
	void shootLong(int playerId, Pos objectivePos);
	void shootArea(int playerId, Pos attackCenterPos);

	void movePlayer(int playerId, Pos newPos);

	Pos getPlayerLocation(int playerId);
	int getPlayerMunicio(int playerId);
	int getPlayerHp(int playerId);

	sf::Packet createPacket();
	void createFromPacket(sf::Packet pack);
};

