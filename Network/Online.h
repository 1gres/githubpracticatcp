#pragma once
#include <SFML/Network.hpp>
#include <thread>
#include <iostream>
#include <mutex>
#include <vector>

#include "ServerRoom.h"

#define SERVER Network::Server::Instance()
#define CLIENT Network::Client::Instance()

#define PEER Network::Peer::Instance()
#define BTSSERVER Network::BootstrapServer::Instance()

namespace Network {
	
	class Peer {
	public:
		static Peer& Instance();
		void Init(void(*funcProtocol)(Peer &_peer, sf::Packet& packet), std::string _ip, short _port);
		void Run(bool debug);

		void Send(int peerIndex, sf::Packet pack);
		

	private:
		Peer();
		void(*FunctionProtocol)(Peer &_peer, sf::Packet& packet);

		void ManagePeers();
		void Debugger();

		sf::SocketSelector socketSelector;
		sf::TcpListener listener;

		int matchedPeersCount;
		std::string ip;
		std::vector<std::pair<std::string, unsigned short>> directions;
		unsigned short connectPort;
		unsigned short localPort;
		sf::TcpSocket socket;
		std::vector<sf::TcpSocket*> sockets;
	};

	class BootstrapServer {
	public:
		static BootstrapServer& Instance();
		void Init(int _maxUsers, short _port);
		void Run();
	private:
		BootstrapServer();
		void ManageSockets();
		sf::TcpListener listener;
		sf::SocketSelector socketSelector;
		std::vector<sf::TcpSocket*> sockets;
		short connectPort;
		int maxUsers;
	};

	class Client {
	public:
		static Client& Instance();
		void Init(void(*funcProtocol)(Client &_client, sf::Packet& packet), std::string _ip, short _port);
		void Run();

		void Send(sf::Packet& packet);

		sf::Packet Recieve();


	private:
		Client();
		void ManageSocket();

		void (*FunctionProtocol)(Client &_client, sf::Packet& packet);

		std::string ip;
		short connectPort;
		sf::TcpSocket socket;

	};
	
	class Server {
	public:
		static Server &Instance();
		void Init(void(*funcProtocol)(Server &_server, sf::Packet& packet, int roomIndex, int socketIndex), short _port, int _maxRooms = 1);
		void Run(bool debug);

		// Returns new room index
		int CreateRoom(int maxClients);

		//Closes a room by index
		void CloseRoom(int index);
		void JoinRoom(int unassignedSocketIndex, int toRoomIndex);
		void ExitRoom(int roomIndex, int socketIndex);
		int RoomsCount();
		ServerRoom &GetRoom(int index);

		void Send(int sock, sf::Packet& packet);
		void Send(int room, int sock, sf::Packet& packet);
		void BroadcastSend(sf::Packet& packet);
		void BroadcastSend(int _roomIndex, sf::Packet& packet);

	private:
		Server();
		void Debugger();
		void ManageSockets();
		sf::TcpSocket &LastSocket();
		int LastSocketIndex();
		
		int maxServerRooms;
		short connectPort;

		//void (*)
		void (*FunctionProtocol)(Server &_server, sf::Packet& packet, int roomIndex, int socketIndex);
		
		sf::TcpListener listener;
		sf::SocketSelector socketSelector;
		
		std::vector<sf::TcpSocket*> unassignedSockets;
		std::vector<ServerRoom> serverRooms;
	};
}