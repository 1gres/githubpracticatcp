#include "Online.h"
#include <iostream>

// -----------------------------------------
// CLIENT
// -----------------------------------------

Network::Client::Client() 
{

}

Network::Client &Network::Client::Instance()
{
	static Client instance;
	return instance;
}


void Network::Client::Init(void(*funcProtocol)(Client &_client, sf::Packet& packet), std::string _ip, short _port)
{
	FunctionProtocol = funcProtocol;
	ip = _ip;
	connectPort = _port;
	socket.connect(ip, connectPort);
}

void Network::Client::Run()
{
	// Receive packets from Server. Use a thread.
	std::thread socketManager(&Client::ManageSocket, this);
	socketManager.detach();
}

void Network::Client::Send(sf::Packet& packet)
{
	socket.send(packet);
}

//RECIEVE PACKET TURN
sf::Packet Network::Client::Recieve()
{	
	sf::Packet packet;
	socket.receive(packet);
	return packet;
}

void Network::Client::ManageSocket()
{
	while (1)
	{
		sf::Packet packet;
		if(socket.receive(packet) == sf::Socket::Status::Done)
			FunctionProtocol(Instance(), packet);
	}
}

// -----------------------------------------
// SERVER
// -----------------------------------------

Network::Server::Server() 
{

}

void Network::Server::Debugger()
{
	while (1) {
		system("cls");
		std::cout << "Unassigned Sockets [" << unassignedSockets.size() << "] " << std::endl;
		std::cout << "Rooms [" << serverRooms.size() << "] " << std::endl;
		for (int i = 0; i < serverRooms.size(); i++) {
			std::cout << "   " << "Room [" << i << "]: Sockets [" << serverRooms[i].GetSocketsCount() << " / "<< serverRooms[i].MaxClients() << "]" << std::endl;
		}

		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

Network::Server &Network::Server::Instance()
{
	static Server instance;
	return instance;
}

void Network::Server::Init(void(*funcProtocol)(Server &_server, sf::Packet& packet, int roomIndex, int socketIndex), short _port, int _maxRooms) {
	maxServerRooms = _maxRooms;
	connectPort = _port;
	FunctionProtocol = funcProtocol;
}

void Network::Server::Run(bool debug)
{
	if (listener.listen(connectPort) == sf::Socket::Status::Done) {
		socketSelector.add(listener);
	}

	std::thread manageSocketThread(&Server::ManageSockets, this);
	manageSocketThread.detach();
	if (debug) {
	std::thread debugger(&Server::Debugger, this);
	debugger.detach();

	}
}

int Network::Server::CreateRoom(int maxClients) {
	serverRooms.push_back(ServerRoom(maxClients));
	return RoomsCount() - 1;
}

void Network::Server::CloseRoom(int index)
{
	for(int i = 0; i < serverRooms[index].GetSocketsCount(); i++)
	{
		unassignedSockets.push_back(&serverRooms[index].PopSocket(i));
	}

	serverRooms.erase(serverRooms.begin() + index);
}

void Network::Server::JoinRoom(int unassignedSocketIndex, int toRoomIndex)
{
	if (toRoomIndex < maxServerRooms) {
		serverRooms[toRoomIndex].AddSocket(unassignedSockets[unassignedSocketIndex]);
		unassignedSockets.erase(unassignedSockets.begin() + unassignedSocketIndex);
	}
}

void Network::Server::ExitRoom(int roomIndex, int socketIndex) {
	sf::TcpSocket &sock = serverRooms[roomIndex].PopSocket(socketIndex);
	unassignedSockets.push_back(&sock);
}

int Network::Server::RoomsCount() {
	return serverRooms.size();
}

ServerRoom & Network::Server::GetRoom(int index)
{
	return serverRooms[index];
	// TODO: insertar una instrucci�n return aqu�
}

void Network::Server::Send(int sock, sf::Packet & packet)
{
	unassignedSockets[sock]->send(packet);
}

void Network::Server::Send(int room, int sock, sf::Packet & packet)
{
	serverRooms[room].GetSocket(sock).send(packet);
}

void Network::Server::BroadcastSend(int _roomIndex, sf::Packet & packet)
{
	for (int i = 0; i < serverRooms[_roomIndex].GetSocketsCount(); i++)
	{
		serverRooms[_roomIndex].GetSocket(i).send(packet);
	}
}

////ENVIAR A CADA SOCKET
//void Network::Server::BroadcastSend(sf::Packet & packet)
//{
//	for (int i = 0; i < unassignedSockets.size(); i++)
//	{
//		unassignedSockets[i]->send(packet);
//	}
//}

void Network::Server::ManageSockets()
{
	while (1) {
		if (socketSelector.wait()) {
			if (socketSelector.isReady(listener) && serverRooms.size() <= maxServerRooms) {
				// The listener is ready: there is a pending connection
				sf::TcpSocket* client = new sf::TcpSocket;
				// Set socket blocking mode
				client->setBlocking(false);
				if (listener.accept(*client) == sf::Socket::Done)
				{
					// Add the new client to the clients list
					std::cout << "Llega el cliente con puerto: " << client->getRemotePort() << std::endl;
					unassignedSockets.push_back(client);
					// Add the new client to the selector so that we will
					// be notified when he sends something
					socketSelector.add(*client);
				}
				else
				{
					// Error, we won't get a new connection, delete the socket
					std::cout << "Error al recoger conexi�n nueva\n";
					delete client;
				}

			}
			else
			{
				for (int i = 0; i < unassignedSockets.size(); i++) {
					if (socketSelector.isReady(*unassignedSockets[i])) {
						sf::Packet packet;
						sf::Socket::Status status = unassignedSockets[i]->receive(packet);
						if (status == sf::Socket::Status::Done || status == sf::Socket::Status::Partial) {
							FunctionProtocol(Instance(), packet, -1, i);
						}
						else if (status == sf::Socket::Status::Disconnected)
						{
							unassignedSockets.erase(unassignedSockets.begin() + i);
						}
						
					}
				}

				for (int i = 0; i < serverRooms.size(); i++) {
					for (int j = 0; j < serverRooms[i].GetSocketsCount(); j++) {
						if (socketSelector.isReady(serverRooms[i].GetSocket(j))) {
							sf::Packet packet;
							sf::Socket::Status status = serverRooms[i].GetSocket(j).receive(packet);
							if (status == sf::Socket::Status::Done || status == sf::Socket::Status::Partial) {
								FunctionProtocol(Instance(), packet, i, j);
							}
							else if (status == sf::Socket::Status::Disconnected)
							{
								serverRooms[i].PopSocket(i);
							}
						}
					}
				}

			}
		}
	}
}

sf::TcpSocket & Network::Server::LastSocket()
{
	return *unassignedSockets.back();
	// TODO: insertar una instrucci�n return aqu�
}

int Network::Server::LastSocketIndex()
{
	return unassignedSockets.size() - 1;
}

// --------------------------
// PEER
// --------------------------

Network::Peer::Peer() {

}

Network::Peer & Network::Peer::Instance()
{
	static Peer instance;
	return instance;
	// TODO: insertar una instrucci�n return aqu�
}

void Network::Peer::Init(void(*funcProtocol)(Peer &_peer, sf::Packet &packet), std::string _ip, short _port)
{
	FunctionProtocol = funcProtocol;
	ip = _ip;
	connectPort = _port;
}

void Network::Peer::Run(bool debug)
{
	if (socket.connect(ip, connectPort) == sf::Socket::Status::Done) {
		socketSelector.add(socket);
		localPort = socket.getLocalPort();
	}


	std::thread peersManager(&Peer::ManagePeers, this);
	peersManager.detach();
	if (debug) {
		std::thread debugger(&Peer::Debugger, this);
		debugger.detach();

	}


}

void Network::Peer::Send(int peerIndex, sf::Packet pack)
{
	sockets[peerIndex]->send(pack);
}

void Network::Peer::ManagePeers()
{
	while (1) {
		if (socketSelector.wait()) {
			if (socketSelector.isReady(listener)) {
				// The listener is ready: there is a pending connection
				sf::TcpSocket* client = new sf::TcpSocket;
				// Set socket blocking mode
				client->setBlocking(false);
				if (listener.accept(*client) == sf::Socket::Done)
				{
					std::cout << "Accepted new Peer!" << std::endl;
					std::cout << "IP: " << client->getRemoteAddress() << "PORT: " << client->getRemotePort() << std::endl;
					socketSelector.add(*client);
					sockets.push_back(client);
				}

			}
			else if (socketSelector.isReady(socket)) {
				//sf::TcpSocket* client = new sf::TcpSocket;
				// Set socket blocking mode
				socket.setBlocking(false);
				
				sf::Packet packet;
				sf::Socket::Status stat = socket.receive(packet);

				

				if (stat == sf::Socket::Done) {
				std::cout << "Received Server Packet" << std::endl;




					int usersNum;
					packet >> usersNum;
					packet >> usersNum;
					std::cout << usersNum << std::endl;

					//std::vector<std::pair<std::string, short>> directions;

					for (int i = 0; i < usersNum; i++) {
						std::string _ip;
						unsigned short _port;
						packet >> _ip >> _port;

						directions.push_back(std::pair<std::string, unsigned short>(_ip, _port));

						std::cout << "Socket [" << i << "] IP: " << _ip << "PORT: " << _port << std::endl;
						//directions.push_back(std::pair<std::string, short>(ip, connectPort));


					}
				
				}
				else if (stat == sf::Socket::Disconnected) {
					std::cout << "Matchmaking Server Disconnected!" << std::endl;
					std::cout << directions.size() << std::endl;
					socket.disconnect();
					listener.setBlocking(false);
					if (listener.listen(localPort) == sf::Socket::Status::Done) {
						socketSelector.add(listener);
					}

					for (int i = 0; i < directions.size(); i++) {
						sf::TcpSocket* sock = new sf::TcpSocket();
						sock->setBlocking(true);
						std::cout << "Storing connection data" << std::endl;
						if (sock->connect(directions[i].first, directions[i].second) == sf::Socket::Done)
						{
							std::cout << "Added Socket: " << "IP: " << sock->getRemoteAddress() << "PORT: " << sock->getRemotePort() << std::endl;
							socketSelector.add(*sock);
							sockets.push_back(sock);
						}
					}

				}
				

			}
			else {
				for (int i = 0; i < sockets.size(); i++) {
					if (socketSelector.isReady(*sockets[i])) {
						// Manage other peer socket
						sf::Packet pack;
						if (sockets[i]->receive(pack) == sf::Socket::Done) {
							FunctionProtocol(Instance(), pack);
						}
					}
				}
			}
		}
	}
}
void Network::Peer::Debugger()
{
	while (1) {
		system("cls");
		std::cout << "Peers [" << sockets.size() << "] " << std::endl;
		for (int i = 0; i < sockets.size(); i++) {
			std::cout << "   " << "IP: " << sockets[i]->getRemoteAddress() << "PORT: " << sockets[i]->getRemotePort() << std::endl;
		}

		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}
// ------------------------------
// Bootstrap Server
// ------------------------------
Network::BootstrapServer::BootstrapServer() {

}

Network::BootstrapServer & Network::BootstrapServer::Instance()
{
	static BootstrapServer instance;
	return instance;
}

void Network::BootstrapServer::Init(int _maxUsers, short _port)
{
	connectPort = _port;
	maxUsers = _maxUsers;
}

void Network::BootstrapServer::Run()
{
	if (listener.listen(connectPort) == sf::Socket::Status::Done) {
		socketSelector.add(listener);
	}

	std::thread manageSocketThread(&BootstrapServer::ManageSockets, this);
	//std::thread debugger(&Server::Debugger, this);
	manageSocketThread.detach();
	//debugger.detach();
}

void Network::BootstrapServer::ManageSockets()
{
	bool runing = true;
	while (runing) {
		if (socketSelector.wait()) {
			if (socketSelector.isReady(listener) && sockets.size() < maxUsers) {
				// The listener is ready: there is a pending connection
				sf::TcpSocket* client = new sf::TcpSocket;
				// Set socket blocking mode
				client->setBlocking(false);
				if (listener.accept(*client) == sf::Socket::Done)
				{
					// Add the new client to the clients list
					std::cout << "Llega el cliente con puerto: " << client->getRemotePort() << std::endl;
					sockets.push_back(client);
					// Add the new client to the selector so that we will
					// be notified when he sends something
					socketSelector.add(*client);

					std::cout << "Connected new socket!" << std::endl;
					std::cout << "Socket count: " << sockets.size() << std::endl;

					sf::Packet pack;
					pack << sockets.size();
					std::cout << sockets.size() << std::endl;
					for (int i = 0; i < sockets.size() - 1; i++) {
						std::cout << "Adding socket data: " << "IP: " << sockets[i]->getRemoteAddress() << "PORT: " << sockets[i]->getRemotePort() << std::endl;
						 pack << sockets[i]->getRemoteAddress().toString() << sockets[i]->getRemotePort();
							

					}
					sockets[sockets.size() - 1]->send(pack);
				}
				else
				{
					// Error, we won't get a new connection, delete the socket
					std::cout << "Error al recoger conexi�n nueva\n";
					delete client;
				}


				// Disconnect when all peers linked
				if (sockets.size() >= maxUsers) {
					std::cout << "All peers connected. Disconnecting matching server..." << std::endl;
					for (int i = 0; i < sockets.size(); i++) {
						sockets[i]->disconnect();
					}

					runing = false;
				}


			}

			for (int i = 0; i < sockets.size(); i++) {
				if (socketSelector.isReady(*sockets[i])) {
					sf::Packet pack;
					if (sockets[i]->receive(pack) == sf::Socket::Disconnected) {
						sockets[i]->disconnect();
					}
				}
			}
		}
	}
}
