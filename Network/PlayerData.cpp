#include "PlayerData.h"



PlayerData::PlayerData()
{
}

PlayerData::PlayerData(int PlayerId)
{
	this->PlayerId = PlayerId;
}


PlayerData::~PlayerData()
{
}

bool PlayerData::isDead()
{
	return actualHp == 0;
}

Pos PlayerData::getPos()
{
	return userLocation;
}

int PlayerData::getActualHp()
{
	return actualHp;
}

int PlayerData::getPlayerId()
{
	return PlayerId;
}

int PlayerData::getMunicio()
{
	return municio;
}

void PlayerData::setPos(Pos newPos)
{
	userLocation = newPos;
}

void PlayerData::setActualHp(int newHp)
{
	actualHp = newHp;
}

void PlayerData::setMunicio(int newMunicio)
{
	municio = newMunicio;
}

void PlayerData::setPlayerId(int playerId)
{
	PlayerId = playerId;
}

void PlayerData::createFromPacket(sf::Packet pack)
{

	pack >> PlayerId;
	pack >> actualHp;
	pack >> municio;
	pack >> userLocation.first;
	pack >> userLocation.second;

}
