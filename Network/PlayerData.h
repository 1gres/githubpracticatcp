#pragma once
#include "States.h"
#include <iostream>
#include <map>
#include "Online.h"

using namespace std;

typedef pair<int, int> Pos;

class PlayerData {
private:
	Pos userLocation;
	int actualHp = initialHp;
	int municio = 0;
	int PlayerId;

public:

	const static int initialHp = 500;

	PlayerData();
	PlayerData(int PlayerId);
	~PlayerData();

	bool isDead();
	Pos getPos();
	int getActualHp();
	int getPlayerId();
	int getMunicio();

	void setPos(Pos newPos);
	void setActualHp(int newHp);
	void setMunicio(int newMunicio);
	void setPlayerId(int playerId);
	
	void createFromPacket(sf::Packet pack);

};
