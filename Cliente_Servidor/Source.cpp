#include "Online.h"
#include "PlayerData.h"
#include "States.h"
#include "GameUpdate.h"
#include <map>


int numActions = 5;
int numPlayers;
int puntosGastados = 0;
int puntosMovimiento;
int turno = 0;

GameUpdate gameUpdate;

std::map<std::pair<int, int>, PlayerData*> playerData;
#define CURRENT_PLAYER playerData[std::pair<int,int>(roomIndex, socketIndex)]

GameState gameState;
PlayerData userData;

void GameProtocol(Network::Server& server, sf::Packet& packet, int roomIndex, int socketIndex) {
	/*if (CURRENT_PLAYER == nullptr) {
		CURRENT_PLAYER = new PlayerData();
		CURRENT_PLAYER->state = GameState::SELECT_ESCENA;
	}*/

	int newState;

	int idx;

	packet >> newState;

	switch (newState)
	{

	case GameState::LISTEN_ROOMS:

		if (server.RoomsCount() >= 0) {

			//server.JoinRoom(socketIndex, server.CreateRoom(numPlayers));
			packet << GameState::RECIEVE_ROOMS;
			int i = server.RoomsCount();
			packet << i;

			std::cout << roomIndex << " " << socketIndex;

			server.Send(socketIndex, packet);
		}
		break;

	case GameState::JOIN_ROOM:

		int salaEnter;
		packet >> salaEnter;

		/*if (server.GetRoom(ro) == server.GetRoom(salaEnter)) {

			
		}
		else {
			cout << "No existe ninguna sala con este valor" << endl;
		}*/

		if (server.GetRoom(salaEnter).GetSocketsCount() < server.GetRoom(salaEnter).MaxClients()) {
			sf::Packet pack;
			pack << GameState::MATCH_ROOM << roomIndex;

			server.Send(socketIndex, pack);

			cout << "Hemos entrado dentro de la partida seleccionada" << endl;

			gameState = GameState::MATCH_ROOM;
			//newState = GameState::MATCH_ROOM;

			server.JoinRoom(socketIndex, salaEnter);
		}



		break;

	case CREATE_ROOM:

		//CREAMOS UNA SALA CON EL TAMA�O QUE NOS HAN PASADO

		packet >> numPlayers;

		idx = server.CreateRoom(numPlayers);

		packet << GameState::MATCH_ROOM;

		server.Send(socketIndex, packet);

		server.JoinRoom(socketIndex, idx);

		//gameState = GameState::MATCH_ROOM;
		//newState = GameState::MATCH_ROOM;

		break;

	case MATCH_ROOM:

		cout << "Patata" << endl;
		/*int roomNumber;

		roomNumber = roomIndex;*/

		if (server.GetRoom(roomIndex).GetSocketsCount() == server.GetRoom(roomIndex).MaxClients()) {

			packet << GameState::GAME_START;
			server.Send(roomIndex, socketIndex, packet);
		}
		else {
			packet << GameState::MATCH_ROOM;
			server.Send(roomIndex, socketIndex, packet);
		}

		break;

	case GameState::GAME:

		int newAction;
		packet >> newAction;

		if (newAction == GameAction::MOVE) {

			//Descomprimir packet de Move amb les accions
			int playerId;
			packet >> playerId;
			int direccio;
			packet >> direccio;
			TypeMove typeMove = (TypeMove)direccio;
			gameUpdate.movePlayer(playerId, typeMove);

			sf::Packet pack = gameUpdate.createPacket();
			server.BroadcastSend(roomIndex, pack);
		}

		if (newAction == TypeAttack::ATTACK_LONG) {
			int playerId;
			packet >> playerId;

			int posX;
			packet >> posX;
			int posY;
			packet >> posY;

			Pos pos(posX, posY);

			gameUpdate.shootLong(playerId, pos);
		}
		else if (newAction == TypeAttack::ATTACK_AREA) {
			int playerId;
			packet >> playerId;

			int posX;
			packet >> posX;
			int posY;
			packet >> posY;

			Pos pos(posX, posY);

			gameUpdate.shootArea(playerId, pos);
		}


	case GameAction::END_TURN: {
		turno = (turno + 1) % server.GetRoom(roomIndex).MaxClients();
		//puntosGastados = 0; --> CLIENT

		sf::Packet packTorn;

		packTorn << NEW_TURN_PLAYER; //canviar estat
		packTorn << gameUpdate.playerIdByPos(turno);

		//Enviar al jugador que ha acabat el torn
		server.BroadcastSend(roomIndex, packTorn);

		break;
	}
	default:
		cout << "ERROR AL ENVIAR EL PAQUETE" << endl;
		break;
	}
}

void main() {
	int in;
	std::cout << "Server(0) or BootstrapServer(1)" << std::endl;
	std::cin >> in;

	if (in == 0) {
		SERVER.Init(GameProtocol, 50000);
		SERVER.Run(true);
	}
	else if (in == 1) {
		BTSSERVER.Init(4, 50000);
		BTSSERVER.Run();
	}


	while (1) {
	}
}