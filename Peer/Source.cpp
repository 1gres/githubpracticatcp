#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Online.h"
#include <sstream>
#include "States.h"
#include "Entity.h"
#include "GameUpdate.h"
using namespace std;


#define WINDOW_TITLE "GAME"
#define FPS_LIMIT 60
#define SCREEN_W 800
#define SCREEN_H 600

int numeroSales;
int PlayerId;
int numeroPlayers;
int numMaxAction = 5;
int actualActions = 0;

bool startGame = false;
bool loginPacketReceived = false;

GameState gameState;
GameAction gameAction;
GameUpdate gameUpdate;
TypeAttack typeAttack;

std::vector<Entity> characters;
typedef pair<int, int> posPlayer;

void GameProtocolFunc(Network::Client &client, sf::Packet& packet) {
	//int i;
	//packet >> i;

	////NECESSARI?
	//gameState = (GameState)i;
	//sf::Packet pack;
	////LO QUE RECIBIMOS

	int state;
	packet >> state;

	switch (state)
	{
	case RECIEVE_ROOMS:

		packet >> numeroSales;
		//i = GameState::JOIN_ROOM;

		int salaEscogida;
		cout << "Hay " << numeroSales << " disponibles. Cual quieres seleccionar?" << endl;
		cin >> salaEscogida;

		packet << GameState::JOIN_ROOM << salaEscogida;

		client.Send(packet);

		break;

	case MATCH_ROOM:
			
		cout << "Buscando jugadores" << endl;
		packet << MATCH_ROOM;
		client.Send(packet);
		break;

	case GAME_START:

		packet >> PlayerId;
		startGame = true;
		break;

	case GAME:

		int stateGame;
		packet >> stateGame;

		if (stateGame == GameAction::MOVE) {
			int newPosX; int newPosY;

			packet >> newPosX;
			packet >> newPosY;

			//EM DE TORNAR A AMPLIAR LES POSICIONS PER  A QUE ENCAIXI TOT
			newPosX = newPosX * 10;
			newPosY = newPosY * 10;

			//characters[i].SetPosition(newPosX, newPosY); 
			//MOVER EL SPRITE QUE TOCA A LA POSICION QUE NOS HA PASADO EL SERVIDOR
			//sprite1.setPosition(newPosX, newPosY);

		}


		break;
	default:
		break;
	}

}

void PeerProtocol(Network::Peer &peer, sf::Packet &packet) {

}

namespace Game {
	sf::RenderWindow window;
	//std::map<unsigned int, GameObject*> characters;
	sf::Event event;
	sf::Texture textures[3];

	void Initialize() {
		window.create(sf::VideoMode(SCREEN_W, SCREEN_H), WINDOW_TITLE);
		//window.setVerticalSyncEnabled(true);
		//window.setFramerateLimit(FPS_LIMIT);
		//gameUpdate = new GameUpdate();

		/*textures[0] = sf::Texture();
		if (!textures[0].loadFromFile("tex.jpg")) {
			std::cout << "Failed loading texture" << std::endl;
		}*/

	}

	void ManageInputs() {

		sf::Packet packetMove;
		packetMove << PlayerId;

		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Up) {
					packetMove << TypeMove::MOVE_UP;
					actualActions = actualActions + 1;
				}
				else if (event.key.code == sf::Keyboard::Down) {
					packetMove << TypeMove::MOVE_DOWN;
					actualActions = actualActions + 1;
				}
				else if (event.key.code == sf::Keyboard::Left) {
					packetMove << TypeMove::MOVE_LEFT;
					actualActions = actualActions + 1;
				}
				else if (event.key.code == sf::Keyboard::Right) {
					packetMove << TypeMove::MOVE_RIGHT;
					actualActions = actualActions + 1;
				}

			}

			if (event.type == sf::Event::MouseButtonPressed) {
				if (event.key.code == sf::Mouse::Left) {
					packetMove << TypeAttack::ATTACK_LONG;

					std::cout << "mouse x: " << event.mouseButton.x / 10 << std::endl;
					std::cout << "mouse y: " << event.mouseButton.y / 10 << std::endl;

					packetMove << event.mouseButton.x << event.mouseButton.y;

					actualActions = actualActions + 2;
				}
				else if (event.key.code == sf::Mouse::Right) {
					packetMove << TypeAttack::ATTACK_AREA;

					std::cout << "mouse x: " << event.mouseButton.x / 10 << std::endl;
					std::cout << "mouse y: " << event.mouseButton.y / 10 << std::endl;

					packetMove << event.mouseButton.x << event.mouseButton.y;

					actualActions = actualActions + 2;
				}
			}

			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Space) {
					actualActions = 5;
				}
			}

		}

		CLIENT.Send(packetMove);
	}

	void Update() {
		// check all the window's events that were triggered since the last iteration of the loop

		sf::Packet pack;
		if (actualActions >= numMaxAction) {
			pack << GameAction::END_TURN;
			actualActions = 0;
		}
		else {
			ManageInputs();
		}

	}



	void Draw() {
		window.clear(sf::Color::Black);
		/*//sf::Sprite* sprites = new sf::Sprite[gameUpdate->getTable().size()];
		sf::Sprite sprite;
		//matrixString& table = gameUpdate->getTable();
		sprite.setTextureRect(sf::IntRect(0, 0, 5, 5));
		sprite.setTexture(textures[0]);
		// Draw stuff here...
		sprite.setPosition(SCREEN_W/2, SCREEN_H/2);
		//window.draw(sprite);*/
		// -------------------

		sf::Vector2f centro;
		int centroX = 50; int centroY = 40;
		centro.x = centroX / 2.f;
		centro.y = centroY / 2.f;

		sf::RectangleShape tanque(sf::Vector2f(50.f, 50.f));
		tanque.setFillColor(sf::Color::Blue);
		tanque.setOrigin(centro.x, centro.y);

		posPlayer playerPosition = gameUpdate.getPlayerLocation(PlayerId);

		//for (int i = 0; i < numeroPlayers; i++) {
		for (map<int, PlayerData>::iterator it = gameUpdate.playersDataInfo.begin(); it != gameUpdate.playersDataInfo.end(); ++it) {

			int initX = it->second.getPos().first;
			int initY = it->second.getPos().second;

			cout << "PosX: " << initX << "PosY: " << initY << endl;

			tanque.setPosition(initX, initY);

			window.draw(tanque);
		}





		window.display();
	}

	void Run() {
		Initialize();
		while (window.isOpen()) {
			Update();
			Draw();
		}
	}
}

void printOptions() {
	//cout << "Me he conectado al servidor" << endl;
	cout << "Bienvenido usuario.Que es lo que quieres hacer ?" << endl;
	cout << "- Crear una partida (1)" << endl;
	cout << "- Unirse a la partida (2)" << endl;
}

void main() {
	int in;
	std::cout << "Client(0) or Peer(1)" << std::endl;
	std::cin >> in;

	if (in == 0) {
		CLIENT.Init(GameProtocolFunc, "localhost", 50000);
		CLIENT.Run();
	}
	else if (in == 1) {
		PEER.Init(PeerProtocol, "localhost", 50000);
		PEER.Run(true);
	}


	int comprobacio;
	sf::Packet packInfo;


	while (!startGame) {
		//LAMADA RECURSIVA
		printOptions();
		cin >> comprobacio;

		switch (comprobacio) {
		case 1:
			cout << "Cuantos jugadores quieres que haya en la partida? (2-4 jugadores)" << endl;
			cin >> numeroPlayers;

			packInfo << GameState::CREATE_ROOM;
			packInfo << numeroPlayers;

			CLIENT.Send(packInfo);
			loginPacketReceived = false;

			break;
		case 2:
			cout << "Buscando partida" << endl;

			packInfo << GameState::LISTEN_ROOMS;

			CLIENT.Send(packInfo);
			loginPacketReceived = false;

			break;
		default:
			cout << "El valor introducido es incorrecto. Vuelve a elegir" << endl;
			loginPacketReceived = true;

			break;
		}

		while (!loginPacketReceived);
		loginPacketReceived = false;
	}

	Game::Run();


}

