#pragma once
#include "SFML/Graphics.hpp"
#include <iostream>

class Entity {
private:
	sf::Sprite sprite;
	sf::Texture tex;
	int width, height;
	sf::Vector2i position;

public:
	Entity(std::string path, int _posX, int _posY, int _w, int _h);
	void SetPosition(int x, int y);
	sf::Vector2i GetPosition();
	void SetRotation(int angle);

	void Update();
	void Draw(sf::RenderWindow &window);


};