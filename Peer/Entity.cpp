#include "Entity.h"

Entity::Entity(std::string path, int _posX, int _posY, int _w, int _h)
{
	position.x = _posX;
	position.y = _posY;
	width = _w;
	height = _h;
	if (!tex.loadFromFile(path))
	{
		std::cout << "No se ha podido cargar la imagen 1" << std::endl;
		system("Pause");
		exit(EXIT_FAILURE);
	}


	sprite.setTexture(tex);
	sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(width, height)));
}

void Entity::SetPosition(int x, int y)
{
	position.x = x;
	position.y = y;
}

sf::Vector2i Entity::GetPosition()
{
	return position;
}

void Entity::SetRotation(int angle)
{
	sprite.setRotation(angle);
}

void Entity::Update()
{
	sprite.move(position.x, position.y);
}

void Entity::Draw(sf::RenderWindow &window)
{
	window.draw(sprite);
}

