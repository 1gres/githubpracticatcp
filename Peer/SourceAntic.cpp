/*#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Online.h"
#include <sstream>
#include "States.h"
#include "Entity.h"

using namespace std;

int numeroSales = 0;
int PlayerId;
bool gameIsRunning = true;

GameState gameState;
GameAction gameAction;
MunicionTanque municionTanque;
std::vector<Entity> characters;

void GameProtocolFunc(Network::Client &client, sf::Packet& packet) {
	int i;
	packet >> i;
	bool startGame = false;
	//NECESSARI?
	gameState = (GameState)i;

	//LO QUE RECIBIMOS
	switch (gameState)
	{
		case WAIT_ROOM:
			packet >> numeroSales;
			i = GameState::JOIN_ROOM;
		break;
		
		case MATCH_ROOM:
			packet >> PlayerId; 
			if (startGame == true) {
				gameState = GameState::GAME;
			}
		break;
			
		case GAME:

			int stateGame;
			packet >> stateGame;

			if (stateGame == GameAction::MOVE) {
				int newPosX; int newPosY;
				packet >> newPosX;
				packet >> newPosY;
				characters[i].SetPosition(newPosX, newPosY);
				//MOVER EL SPRITE QUE TOCA A LA POSICION QUE NOS HA PASADO EL SERVIDOR
				//sprite1.setPosition(newPosX, newPosY);

			}
			else if (stateGame == GameAction::COMBAT) {
			}

			break;
		default:
			break;
	}

}

void PeerProtocol(Network::Peer &peer, sf::Packet &packet) {

}

void main() {
	int in;
	std::cout << "Client(0) or Peer(1)" << std::endl;
	std::cin >> in;

	if (in == 0) {
		CLIENT.Init(GameProtocolFunc, "localhost", 50000);
		CLIENT.Run();
	}
	else if (in == 1) {
		PEER.Init(PeerProtocol, "localhost", 50000);
		PEER.Run(true);
	}




	// Creamos una textura
	sf::Texture textura1;
	sf::Texture textura2;
	sf::Texture textura3;
	sf::Texture textura4;
	sf::Texture texturaCorazon;
	sf::Texture texturaAmmo;

	//Definimos la fuente para los textos
	sf::Font font;
	if (!font.loadFromFile("../../deps64/resources/courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	std::string texturePaths[4]{
		"../../deps64/resources/Tank1.jpg",
		"../../deps64/resources/Tank2.jpg",
		"../../deps64/resources/Tank3.jpg",
		"../../deps64/resources/Tank4.jpg"
	};

	std::vector<sf::Vector2i> initialPositions{
		{0,0},
		{0,0},
		{0,0},
		{0,0}
	};



	for (int i = 0; i < 4; i++) {
		characters.push_back(Entity(texturePaths[i], initialPositions[i].x, initialPositions[i].y, 100, 100));
	}
	// Creamos un sprite para cada textura 
	/*sf::Sprite sprite1;
	sf::Sprite sprite2;
	sf::Sprite sprite3;
	sf::Sprite sprite4;
	sf::Sprite spriteCorazon1;
	sf::Sprite spriteCorazon2;
	sf::Sprite spriteCorazon3;
	sf::Sprite spriteCorazon4;
	sf::Sprite spriteAmmo;

	//Creamos objetos para las escenas
	sf::RectangleShape rectanguloStart(sf::Vector2f(250.f, 120.f));
	sf::RectangleShape rectanguloJoin(sf::Vector2f(320.f, 120.f));
	sf::RectangleShape rectanguloCreate(sf::Vector2f(350.f, 120.f));
	sf::RectangleShape rectanguloCreateRoom(sf::Vector2f(250.f, 90.f));
	sf::RectangleShape rectanguloJoinRoom(sf::Vector2f(100.f, 100.f));

	//Cuadrados OPCIONES
	sf::RectangleShape rectanguloOpcion2(sf::Vector2f(80.f, 80.f));
	sf::RectangleShape rectanguloOpcion3(sf::Vector2f(80.f, 80.f));
	sf::RectangleShape rectanguloOpcion4(sf::Vector2f(80.f, 80.f));

	//Creamos objectos del escenario

	//Creamos los mensajes para las escenas
	sf::Text textoStart("START", font, 55);
	sf::Text textoGameName("Tank Game", font, 150);
	sf::Text textoJoin("Create game", font, 50);
	sf::Text textoJoinLoading("Loading the rooms...", font, 60);
	sf::Text textoCreate("Join game", font, 50);
	sf::Text textoCreateGame("Create", font, 60);
	sf::Text textoWaitingPlayers("Waiting players...", font, 60);

	//Texto OPCIONES
	sf::Text textoOpcionJugadores("1- Cuantos jugadores vais a jugar?", font, 25);
	sf::Text textoOpcion2("2", font, 70);
	sf::Text textoOpcion3("3", font, 70);
	sf::Text textoOpcion4("4", font, 70);


	// Asignamos la textura al sprite
	/*sprite1.setTexture(textura1);
	sprite2.setTexture(textura2);
	sprite3.setTexture(textura3);
	sprite4.setTexture(textura4);
	spriteCorazon1.setTexture(texturaCorazon);
	spriteCorazon2.setTexture(texturaCorazon);
	spriteCorazon3.setTexture(texturaCorazon);
	spriteCorazon4.setTexture(texturaCorazon);
	spriteAmmo.setTexture(texturaAmmo);

	// Seleccionamos solo un rectangulo para pintar la textura
	//Sprites
	/*sprite1.setTextureRect(sf::IntRect(0, 0, 260, 135));
	sprite2.setTextureRect(sf::IntRect(0, 0, 500, 235));
	sprite3.setTextureRect(sf::IntRect(0, 0, 330, 150));
	sprite4.setTextureRect(sf::IntRect(0, 0, 480, 270));
	spriteCorazon1.setTextureRect(sf::IntRect(0, 0, 270, 250));
	spriteCorazon2.setTextureRect(sf::IntRect(0, 0, 270, 250));
	spriteCorazon3.setTextureRect(sf::IntRect(0, 0, 270, 250));
	spriteCorazon4.setTextureRect(sf::IntRect(0, 0, 270, 250));
	spriteAmmo.setTextureRect(sf::IntRect(0, 0, 430, 480));

	//Rectangulos
	rectanguloStart.setFillColor(sf::Color::Yellow);
	rectanguloJoin.setFillColor(sf::Color::Yellow);
	rectanguloCreate.setFillColor(sf::Color::Yellow);
	rectanguloCreateRoom.setFillColor(sf::Color::Yellow);
	rectanguloJoinRoom.setFillColor(sf::Color::Yellow);

	//Opciones
	rectanguloOpcion2.setFillColor(sf::Color::Magenta);
	rectanguloOpcion3.setFillColor(sf::Color::Magenta);
	rectanguloOpcion4.setFillColor(sf::Color::Magenta);


	// Movemos el sprite a la posicion deseada
	//Sprite
	//sprite1.move(playerData.sprite1X, playerData.sprite1Y);
	//int sprite1XIni = playerData.sprite1X - 30;
	//int sprite1YIni = playerData.sprite1Y - 50;
	////spriteCorazon1.move(sprite1X, sprite1Y - 50);

	//sprite2.move(playerData.sprite2X, playerData.sprite2Y);
	//spriteCorazon2.move(playerData.sprite2X, playerData.sprite2Y - 35);

	//sprite3.move(playerData.sprite3X, playerData.sprite3Y);
	//spriteCorazon3.move(playerData.sprite3X, playerData.sprite3Y - 50);

	//sprite4.move(playerData.sprite4X, playerData.sprite4Y);
	//spriteCorazon4.move(playerData.sprite4X, playerData.sprite4Y - 35);

	spriteAmmo.move(500, 400);

	//Rectangulos
	rectanguloStart.setPosition(500, 550);
	rectanguloJoin.setPosition(225, 575);
	rectanguloCreate.setPosition(735, 575);
	rectanguloCreateRoom.setPosition(500, 750);
	

	//Opciones
	rectanguloOpcion2.setPosition(220, 270);
	rectanguloOpcion3.setPosition(520, 270);
	rectanguloOpcion4.setPosition(820, 270);


	//Textos
	textoStart.setPosition(540, 570);
	textoGameName.setPosition(250, 100);
	textoCreate.setPosition(250, 600);
	textoJoin.setPosition(750, 600);
	textoJoinLoading.setPosition(250, 500);
	textoWaitingPlayers.setPosition(300, 500);
	textoCreateGame.setPosition(520, 750);

	//Opciones
	textoOpcion2.setPosition(240, 260);
	textoOpcion3.setPosition(540, 260);
	textoOpcion4.setPosition(840, 260);
	textoOpcionJugadores.setPosition(150, 220);

	// Cambiamos el origen al centro del sprite
	sf::Vector2f centro;
	int centroX = 250; int centroY = 150;
	centro.x = centroX / 2.f;
	centro.y = centroY / 2.f;

	//Asignamos el punto central a todos los sprites
	//Sprite
	/*sprite1.setOrigin(centro);
	sprite2.setOrigin(centro);
	sprite3.setOrigin(centro);
	sprite4.setOrigin(centro);
	spriteCorazon1.setOrigin(centro);
	spriteCorazon2.setOrigin(centro);
	spriteCorazon3.setOrigin(centro);
	spriteCorazon4.setOrigin(centro);
	spriteAmmo.setOrigin(centro);

	//Rectangulos
	rectanguloStart.setOrigin(centro);
	rectanguloJoin.setOrigin(centro);
	rectanguloCreate.setOrigin(centro);
	rectanguloCreateRoom.setOrigin(centro);
	rectanguloJoinRoom.setOrigin(centro);

	//Opciones
	rectanguloOpcion2.setOrigin(centro);
	rectanguloOpcion3.setOrigin(centro);
	rectanguloOpcion4.setOrigin(centro);

	//Textos
	textoStart.setOrigin(centro);
	textoGameName.setOrigin(centro);
	textoCreate.setOrigin(centro);
	textoJoin.setOrigin(centro);
	textoJoinLoading.setOrigin(centro);
	textoCreateGame.setOrigin(centro);
	textoWaitingPlayers.setOrigin(centro);

	//Opciones
	textoOpcion2.setOrigin(centro);
	textoOpcion3.setOrigin(centro);
	textoOpcion4.setOrigin(centro);
	textoOpcionJugadores.setOrigin(centro);

	//Rotamos los sprites
	//sprite1.rotate(180);
	//sprite3.rotate(180);

	//// Escalamos el sprite
	//sprite1.setScale(0.5f, 0.5f);
	//sprite2.setScale(0.25f, 0.25f);
	//sprite3.setScale(0.4f, 0.4f);
	//sprite4.setScale(0.25f, 0.25f);
	//spriteCorazon1.setScale(0.1f, 0.1f);
	//spriteCorazon2.setScale(0.1f, 0.1f);
	//spriteCorazon3.setScale(0.1f, 0.1f);
	//spriteCorazon4.setScale(0.1f, 0.1f);
	spriteAmmo.setScale(0.1f, 0.1f);


	//Variable Juego
	//Vida tanque
	int vida = 3;
	   	
	//Definimos el vector para la dimension de la ventana
	sf::Vector2i screenDimensions(1000, 1000);

	// Crea una ventana 
	sf::RenderWindow window(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Tank Game");

	// Activa la sincronizaci�n vertical (60 fps)
	window.setVerticalSyncEnabled(true);

	// Creamos un objeto evento
	sf::Event event;

	//Variables a la hora de crear nuestro juego
	int numeroJugadores = 2;

	//Variables de las salas que existen
	//int numSala[];



	// Game Loop mientras la ventana est� abierta
	while (window.isOpen()) {

		//Paquete que enviamos al servidor
		sf::Packet packSend;

		switch (gameState) {
		case GameState::START:
			cout << "Escena 1: Inicio juego" << endl;
			//cout << numeroJugadores << endl;

			// Procesamos la pila de eventos
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::MouseButtonPressed)
				{

					if (event.mouseButton.button == sf::Mouse::Left)
					{
						// Comprobamos la posicion del raton
						std::cout << "mouse x: " << event.mouseButton.x << std::endl;
						std::cout << "mouse y: " << event.mouseButton.y << std::endl;
						if (event.mouseButton.x > 370 && event.mouseButton.x < 620) {
							if (event.mouseButton.y > 480 && event.mouseButton.y < 600)
							{ 
								gameState = GameState::SELECT_ESCENA;
							}

						}
												
					}
				}
				if (event.key.code == sf::Keyboard::Escape) {
					window.close();
					exit(EXIT_FAILURE);
				}
			}

			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos
		
			window.draw(rectanguloStart);
			window.draw(textoStart);
			window.draw(textoGameName);
					
			// Actualizamos la ventana
			window.display();


			break;
		case GameState::SELECT_ESCENA:
			cout << "Escena 2: Seleccionar" << endl;

			// Procesamos la pila de eventos
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::MouseButtonPressed)
				{

					if (event.mouseButton.button == sf::Mouse::Left)
					{
						std::cout << "mouse x: " << event.mouseButton.x << std::endl;
						std::cout << "mouse y: " << event.mouseButton.y << std::endl;

						if (event.mouseButton.x > 95 && event.mouseButton.x < 415) {
							if (event.mouseButton.y > 505 && event.mouseButton.y < 625)
							{
								

								//gameState = GameState::JOIN_ROOM;
								gameState = GameState::WAIT_ROOM;
								//window.close();
							}

						}
						else if (event.mouseButton.x > 605 && event.mouseButton.x < 955) {
							if (event.mouseButton.y > 505 && event.mouseButton.y < 625)
							{

								gameState = GameState::CREATE_ROOM;
								//window.close();
							}

						}

					}
					
				}
				// Si el evento es de tipo Closed cerramos la ventana
				if (event.key.code == sf::Keyboard::Escape) {
					gameState = GameState::START;
					/*window.close();
					exit(EXIT_FAILURE);
				}

			}


			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos

			window.draw(rectanguloCreate);
			window.draw(rectanguloJoin);
			window.draw(textoGameName);
			window.draw(textoCreate);
			window.draw(textoJoin);

			// Actualizamos la ventana
			window.display();


			break;
		case GameState::WAIT_ROOM:
			
			while (window.pollEvent(event))
			{
				cout << "Waiting rooms from server" << endl;
				
				/*if (event.type == sf::Event::MouseButtonPressed)
				{

					if (event.mouseButton.button == sf::Mouse::Left)
					{
						std::cout << "mouse x: " << event.mouseButton.x << std::endl;
						std::cout << "mouse y: " << event.mouseButton.y << std::endl;

						gameState = GameState::JOIN_ROOM;

					}

				}
				

			}

			packSend << GameState::WAIT_ROOM << GameAction::LISTEN_ROOMS;

			CLIENT.Send(packSend);
			
			
			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos
			window.draw(textoJoinLoading);
			window.draw(textoGameName);

			// Actualizamos la ventana
			window.display();

			break;

		case GameState::JOIN_ROOM:
			cout << "Escena 4: Join Game" << endl;


			
		
			// Procesamos la pila de eventos
			while (window.pollEvent(event))
			{


				if (event.type == sf::Event::MouseButtonPressed)
				{
							
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						std::cout << "mouse x: " << event.mouseButton.x << std::endl;
						std::cout << "mouse y: " << event.mouseButton.y << std::endl;
												
						// HAS DE MIRAR A QUINA SALA ENTRES
						packSend << GameState::MATCH_ROOM; //<< numSala;

					}
					
				}
				// Si el evento es de tipo Closed cerramos la ventana
				if (event.key.code == sf::Keyboard::Escape) {
					gameState = GameState::SELECT_ESCENA;
					/*window.close();
					exit(EXIT_FAILURE);
				}

			}

			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos
			window.draw(textoGameName);
			if (numeroSales <= 6) {
				for (int i = 0; i < numeroSales; i++) {

					int tmp = i / 2;
					//cout << tmp << endl;
					int posY = 350;;

					if (i % 2 == 0) {
						rectanguloJoinRoom.setPosition(250 + 350 * tmp, 400);
						window.draw(rectanguloJoinRoom);

					}
					else if (i % 2 != 0) {
						rectanguloJoinRoom.setPosition(250 + posY * tmp, 400 + 200);
						window.draw(rectanguloJoinRoom);
					}


				}
			}
			else
			{
				cout << "Error en la consola porque se intenta crear una 7� sala y no puede" << endl;
				system("Pause");
				gameState = GameState::SELECT_ESCENA;
			}
			
			// Actualizamos la ventana
			window.display();


			break;

		case GameState::CREATE_ROOM:
			cout << "Escena 5: Create Game" << endl;

			textoGameName.setPosition(250, 50);

			packSend << GameState::CREATE_ROOM;
			
			// Procesamos la pila de eventos
			while (window.pollEvent(event))
			{

				switch (event.type)
				{
					case sf::Event::MouseButtonPressed:
					{

						if (event.mouseButton.button == sf::Mouse::Left)
						{

							std::cout << "mouse x: " << event.mouseButton.x << std::endl;
							std::cout << "mouse y: " << event.mouseButton.y << std::endl;

							//OPCIONES
							//Seleccion de jugadores
							// 2 JUGADORES
							if (event.mouseButton.x > 90 && event.mouseButton.x < 170) {
								if (event.mouseButton.y > 200 && event.mouseButton.y < 280)
								{
									rectanguloOpcion2.setFillColor(sf::Color::Green);
									rectanguloOpcion3.setFillColor(sf::Color::Red);
									rectanguloOpcion4.setFillColor(sf::Color::Red);
									numeroJugadores = 2;
									//window.close();
								}

							}
							// 3 JUGADORES
							else if (event.mouseButton.x > 390 && event.mouseButton.x < 470) {
								if (event.mouseButton.y > 200 && event.mouseButton.y < 280)
								{
									rectanguloOpcion2.setFillColor(sf::Color::Red);
									rectanguloOpcion3.setFillColor(sf::Color::Green);
									rectanguloOpcion4.setFillColor(sf::Color::Red);
									numeroJugadores = 3;
									//window.close();
								}

							}
							// 4 JUGADORES
							else if (event.mouseButton.x > 690 && event.mouseButton.x < 770) {
								if (event.mouseButton.y > 200 && event.mouseButton.y < 280)
								{
									rectanguloOpcion2.setFillColor(sf::Color::Red);
									rectanguloOpcion3.setFillColor(sf::Color::Red);
									rectanguloOpcion4.setFillColor(sf::Color::Green);
									numeroJugadores = 4;
									//window.close();
								}

							}
							//Empezar el juego
							else if (event.mouseButton.x > 370 && event.mouseButton.x < 620) {
								if (event.mouseButton.y > 680 && event.mouseButton.y < 770)
								{
									packSend << GameAction::CREATE_GAME << numeroJugadores;
									CLIENT.Send(packSend);
								
									gameState = GameState::MATCH_ROOM;
									//gameState = GameState::GAME;

								}

							}

						}
						// Si el evento es de tipo Closed cerramos la ventana
						if (event.key.code == sf::Keyboard::Escape) {
							gameState = GameState::SELECT_ESCENA;
							/*window.close();
							exit(EXIT_FAILURE);
						}
					}

				}

			}

			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos

			window.draw(rectanguloCreateRoom);
			window.draw(textoGameName);
			window.draw(textoCreateGame);

			//OPCIONES
			window.draw(textoOpcionJugadores);
			window.draw(rectanguloOpcion2);
			window.draw(rectanguloOpcion3);
			window.draw(rectanguloOpcion4);
			window.draw(textoOpcion2);
			window.draw(textoOpcion3);
			window.draw(textoOpcion4);


			// Actualizamos la ventana
			window.display();


			break;

		case GameState::MATCH_ROOM:
			
			while (window.pollEvent(event))
			{
				cout << "Game Created...Waiting players" << endl;

				/*if (event.type == sf::Event::MouseButtonPressed)
				{

					if (event.mouseButton.button == sf::Mouse::Left)
					{
						std::cout << "mouse x: " << event.mouseButton.x << std::endl;
						std::cout << "mouse y: " << event.mouseButton.y << std::endl;

						gameState = GameState::GAME;

					}

				}


			}
			
			packSend << GameState::MATCH_ROOM;
			CLIENT.Send(packSend);
						
			// Pintamos la pantalla
			window.clear(sf::Color(50, 50, 50));

			//Pintamos los objetos
			window.draw(textoWaitingPlayers);
			window.draw(textoGameName);

			// Actualizamos la ventana
			window.display();

			break;
						
		case GameState::GAME:

			packSend << GameState::GAME;

			cout << "Escena Juego" << endl;

			while (gameIsRunning) {// Procesamos la pila de eventos

				sf::Packet PackTurn = CLIENT.Recieve();
				int myTurn;
				PackTurn >> myTurn;

				if (PlayerId == myTurn) {
					while (window.pollEvent(event))
					{
						if (event.type == sf::Event::MouseButtonPressed)
						{

							if (event.mouseButton.button == sf::Mouse::Left)
							{
								// Comprobamos la posicion del raton
								std::cout << "mouse x: " << event.mouseButton.x << std::endl;
								std::cout << "mouse y: " << event.mouseButton.y << std::endl;

								packSend << GameAction::MOVE << event.mouseButton.x << event.mouseButton.y;
								CLIENT.Send(packSend);

							}
						}

						if (event.type == sf::Event::KeyPressed)
						{
							packSend << GameAction::COMBAT;

							if (event.key.code == sf::Keyboard::Q) {
								cout << "HAS SELECCIONADO LA MUNICION EN LINEA RECTA" << endl;

								municionTanque = MunicionTanque::MUNICION_RECTA;
								packSend << municionTanque;

								CLIENT.Send(packSend);

								system("pause");

							}
							else if (event.key.code == sf::Keyboard::W) {
								cout << "HAS SELECCIONADO LA MUNICION EN DIAGONAL" << endl;

								municionTanque = MunicionTanque::MUNICION_DIAGONAL;
								packSend << municionTanque;

								CLIENT.Send(packSend);

								system("pause");
							}

						}
						if (event.type == sf::Event::KeyPressed) {
							if (event.key.code == sf::Keyboard::P) {
								cout << "HAS PASADO TU TURNO " << endl;
								packSend << GameAction::PASS_TURN;
								CLIENT.Send(packSend);
								system("pause");
							}
						}


						// Si el evento es de tipo Closed cerramos la ventana

						if (event.key.code == sf::Keyboard::Escape) {
							gameState = GameState::SELECT_ESCENA;
							/*window.close();
							exit(EXIT_FAILURE);
						}

					}
				}
				
			}
			

			// Pintamos la pantalla
			window.clear(sf::Color(102, 66, 33));
			window.draw(spriteAmmo);

			for (int i = 0; i < numeroJugadores; i++) {
				characters[i].Draw(window);


			}

			// Actualizamos la ventana
			window.display();


			break;


		default:
			cout << "Error en el juego. Se va a cerrar por seguridad" << endl;
			system("Pause");
			exit(EXIT_FAILURE);
			break;
		}
	}


}*/